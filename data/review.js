var mongoose = require('mongoose');

//info based on a room someone would rent
var reviewSchema = new mongoose.Schema({
    overall : {type : Number, required : true},
    price : {type : Number, required : true},
    distance: {type : Number, required : true},
    parking :{type : Number, required : true},
    authorName :{type : String, required : true},
    authorID : Number
});

//info about a specific site
var apartmentSchema = new mongoose.Schema({
    name: {type : String , unique : true, required : true},
    address: String,
    reviews: [reviewSchema]
});

//info about a university
var campusSchema = new mongoose.Schema({
    name: {type : String , unique : true, required : true},
    apartments : [apartmentSchema] 
});

//unique list of campus names
campusSchema.statics.getCampuses = function(cb){
    return this.model('Campus').distinct('name', cb);
};

//guarentee campus exists.
campusSchema.statics.findCampus = function(name, cb){
    var type = this;
    this.findOne({name: name}, function(err, campus){
        if(err){
            cb(err, null);
            return;
        }

        if(campus){
            console.log("Campus already exists:\n", campus);
            cb(null);
            return;
        }
        
        //fuck need to make a new entry
        var newCampus = new type({name : name});
        newCampus.save(function(err){
            if(err){
                cb(err);
                return;
            }
            console.log("Campus created:\n", newCampus);
            cb(null);
            return;
        });
    });
};

//find apartment record. create if necesary
campusSchema.statics.findApt = function(campusName, aptName, cb){
    var type = this;
    var conditions = {"name" : campusName, "apartments.name" : aptName};
    console.log("\tCheck Apt conditions:\n", conditions, "\n");
    this.findOne(conditions,function(err, doc){
        if(err){
            cb(err);
            return;
        }
        console.log("\t",doc,"\n");
        if(!doc){
            console.log('Apartment was not found. need to create one');
            type.findOne({name : campusName}, function(err, campus){
                if(err){
                    cb(err);
                    return;
                }

                if(!campus){
                    cb(new Error("Camus needs to be created with find campus"));
                    return;
                }

                var Apt = mongoose.model('Apartment', apartmentSchema);
                var newApt = new Apt({name : aptName});
                
                campus.apartments.push(newApt);
                campus.save(function(err){
                    if(err){
                        cb(err, null);
                        return;
                    }
                    console.log("Apt created:\n", campus)
                    cb(null);
                    return;
                });
            });
        }else{ 
            console.log("Apt already existed:\n", doc)
            cb(null);
            return;
        }
    });
};

campusSchema.statics.addReview = function(campusName, aptName, reviewInfo, cb){
    var conditions = {name : campusName, "apartments.name" : aptName};
    this.findOne(conditions,function(err, doc){
        if(!doc){
            cb(new Error("Cannot find apartment/campus combo"));
            return;
        }
    
        var Review = mongoose.model('Review', reviewSchema);
        var newReview = new Review(reviewInfo);
       
        var apartment = null; 
        for(var i in doc.apartments){
            var apartment = doc.apartments[i];
            if(apartment.name == aptName){
                apartment = apartment;
                break;    
            }
        }
        if(apartment){
            apartment.reviews.push(newReview);
            doc.save(function(err){
                if(err){
                    cb(err);
                    return;
                }

                console.log("Created new review:\n",doc,"\n",apartment);
                cb(null);
                return;
            });
        }else{
            cb(new Err('Query says apartment exits, but cannot find by name'));
            return;
        }
    });
};

//update to return dictionary of arrays campus -> apartments
campusSchema.statics.getCampAptTable = function(cb){
    this.find({}, function(err, campuses){
        if(err){
            cb(err, null);
            return;
        }
        
        var table = {};
        for(var i in campuses){
            var campus = campuses[i];
            console.log("Found " + campus.name);
            console.log("Found apartments:\n", campus.apartments);
            table[campus.name] = [];
            for(var j = 0; j < campus.apartments.length; j ++){
                var apt = campus.apartments[j];
                table[campus.name].push(apt.name);
            }
        }
        //console.log("Campus => Apartments table:\n",table);
        cb(null, table);
        return;
    });
};

//create dict aptname -> [array object]
campusSchema.statics.getReviews = function(campusName, aptName, cb){
    var aptRegex = new RegExp('^' + aptName + '$', "i");
    this.findOne({name : campusName}, function(err, campus){
        if(err){
            cb(err, null);
            return;
        }

        var table = {};
        for(var i = 0; i < campus.apartments.length; i++){
            var apt = campus.apartments[i];
            if(aptRegex.test(apt.name)){
                table[apt.name] = [];
                for(var j = 0; j < apt.reviews.length; j++){
                    var review = apt.reviews[j];
                    var info = [apt.name, review.overall, review.price, review.distance, review.parking];
                    table[apt.name].push(info);
                }
            }
        }
        console.log(table);
        cb(null, table);
    });
};

var Campus = mongoose.model('Campus', campusSchema);
module.exports = Campus;

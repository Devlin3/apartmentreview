var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Campus = require("./data/review");

console.log("Insided unit test");
mongoose.connect('mongodb://localhost/devUnitTest1');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(){console.log('DB connected')});

var date = new Date();
var time = date.getTime();
campusName = "Oregon State" + time;
aptName = "Da crib" + time;
Campus.findCampus(campusName, function(err){
    Campus.findApt(campusName, aptName, function(err, apt){
        Campus.addReview(campusName, aptName, {overall: 1, price: 200.00, distance : 2, authorName : "JD", authorID : 69, parking: 4}, function(err){
            Campus.findCampus(campusName, function(err){
                Campus.findApt(campusName, aptName, function(err, apt){
                    Campus.addReview(campusName, aptName, {overall: 1, price: 200.00, distance : 2, authorName : "JD", authorID : 69, parking: 4}, function(err){
                
                        Campus.getCampAptTable(function(err, data){
                            console.log(data);
                            Campus.find(function(err, db){
                                console.log("");
                                for(var i = 0; i < db.length; i++){
                                    var campus = db[i];
                                    console.log("Campus name: ", campus.name);
                                    for(var j = 0; j < campus.apartments.length; j ++){
                                        var apartment = campus.apartments[j];
                                        console.log("\tApartment Name: ",apartment.name);
                                        for(var k = 0; k < apartment.reviews.length; k++){
                                            var review = apartment.reviews[k];
                                            console.log("\t\tREview: ",review);
                                        }
                                    }
                                }
                                Campus.getReviews(campusName, ".*", function(err,data){
                                    console.log(data);
                                    Campus.remove({},function(){});
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});

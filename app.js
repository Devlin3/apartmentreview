var express = require('express');

//database info
var mongoose = require('mongoose');

//add third party authentication
var passport = require('passport');
var Strategy = require('passport-facebook').Strategy

var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var auth = require('./routes/auth');
var actions = require('./routes/actions');


var app = express();

//connect to datbase
mongoose.Promise = global.Promise;
var url = 'mongodb://localhost:27017/dev3';
mongoose.connect(url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'DB conn error:'));
db.once('open', function(){
    console.log('Connected to DB');
});

//OAuth 2.0 authentication
passport.use(new Strategy({
    clientID: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    callbackURL: 'http://138.197.143.230:3000/auth/login/facebook/return'
},function(accessToken, refreshToken, profile, cb){
    return cb(null, profile);
}));
passport.serializeUser(function(user, cb){
    cb(null, user);
});
passport.deserializeUser(function(obj, cb){
    cb(null, obj);
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser(process.env.COOKIE_SECRET));
app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', routes);
app.use('/auth', auth);
app.use('/actions', actions);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;

var express = require('express');
var router = express.Router();
var passport = require('passport');

router.get('/login',
    function(req, res){
        res.render('login');
    }
);

router.get('/login/facebook',
    passport.authenticate('facebook')
);

router.get('/login/facebook/return',
    passport.authenticate('facebook', {successReturnToOrRedirect: '/', falureRedirect: '/login'}),
    function(req, res){
        res.render('profile', {user: req.user});
    }
);

router.get('/logout',
    function(req, res){
        req.logout();
        console.log(req.session);
        res.render('home', {user: req.user});
    }
);

module.exports = router;

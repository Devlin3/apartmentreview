var express = require('express');
var router = express.Router();
var passport = require('passport');

var assert = require('assert');
var Campus = require('../data/review');

router.get('/viewProfile',
    require('connect-ensure-login').ensureLoggedIn('/auth/login'),
    function(req, res){
        res.render('profile', {user: req.user});
    }
);

router.get('/newReview',
    require('connect-ensure-login').ensureLoggedIn('/auth/login'),
    function(req, res){
        Campus.getCampAptTable(function(err, data){
            if(err){
                console.log(err);
                throw(err);
            }
            console.log(data);
            res.render('newReview',{table: data,  submitted: null});
        });
    }
);

router.post('/newReview',
    require('connect-ensure-login').ensureLoggedIn('/auth/login'),
    function(req, res){
        form = req.body;
        console.log(form);        
        //Form not fully filled out
        if(Object.keys(form).length != 6){
            res.render('newReview', {submitted: false});
            return;
        }
        for(var key in form){
            if(form[key] == null || form[key] == ''){
                Campus.getCampAptTable(function(err, data){
                    if(err) throw(err);

                    res.render('newReview', {table: data, submitted: false});
                    return;
                });
            }
        } 
        console.log("ready to make a new entry");

        Campus.findCampus(form.campus, function(err){
            if(err) throw(err);
            console.log("Sucessfully confirmed campus exists");
            Campus.findApt(form.campus, form.name, function(err){
                if(err) throw(err);
                console.log("Sucessfully confirmed apt exists");

                //make value copy of form
                var reviewInfo = JSON.parse(JSON.stringify(form));
                
                //modify to match review schema
                delete reviewInfo["name"];
                delete reviewInfo["campus"];
                reviewInfo["authorName"] = req.user.displayName;
                reviewInfo["authorID"] = req.user.id;

                Campus.addReview(form.campus, form.name, reviewInfo, function(err){
                    if(err) throw(err);
                    console.log("Successfuly created review");
                    Campus.getCampAptTable(function(err, data){
                        if(err) throw(err);
                        res.render('newReview', {table: data, submitted : true});
                    });
                });
            });
        });
    }
);

router.get('/viewApartments',
    require('connect-ensure-login').ensureLoggedIn('/auth/login'),
    function(req, res){
        Campus.getCampuses(function(err, campuses){
            if(err){
                console.log(err);
                throw(err);
            }
            res.render('viewApartments', {campuses: campuses, data: null});
        });
    }
);
router.post('/viewApartments',
    require('connect-ensure-login').ensureLoggedIn('/auth/login'),
    function(req, res){
        form = req.body;
        console.log(form);        
        
        //Form not fully filled out
        if(Object.keys(form).length != 1){
            Campus.getCampuses(function(err, campuses){
                if(err){
                    console.log(err);
                    throw(err);
                }
                res.render('viewApartments', {campuses: campuses, data: null});
            });
        }
        
        var results = Campus.getReviews(form.campus,".*", function(error, table){
            if(error){
                throw(err);
            }
            
            console.log(table);
            var data = [];
            for(var name in table){
                console.log(name);
                var reviews = table[name];
                for(var i=0; i < reviews.length; i++){
                    data.push(reviews[i]);
                }
            }
            console.log(data)
            Campus.getCampuses(function(err, campuses){
                if(err){
                    console.log(err);
                    throw(err);
                }
                res.render('viewApartments', {campuses: campuses, data: data});
            });
        });
    }
);

module.exports = router;
